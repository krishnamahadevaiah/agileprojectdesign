package  
{
        /**
         * ...
         * @author Krsihna
         */
        public class Game 
        {
                private var board:Board;
                
                public function Game() 
                {
                        trace("new game created");
                        board = new Board();
                        
                        board.draw();
                        board.startGame();
                        
                }
                
        }

}